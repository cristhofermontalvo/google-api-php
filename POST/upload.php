<?php
require_once '../vendor/autoload.php';

session_start();
$archivo=$_FILES['file'];
//var_dump($file);
// use strrevpos function in case your php version does not include it
function strrevpos($instr, $needle)
{
    $rev_pos = strpos (strrev($instr), strrev($needle));
    if ($rev_pos===false) return false;
    else return strlen($instr) - $rev_pos - strlen($needle);
};

function before_last ($este, $inthat)
    {
        return substr($inthat, 0, strrevpos($inthat, $este));
    };

;

$client = new Google_Client();
$client->setAuthConfig('../client_secret.json');
$client->addScope(Google_Service_Drive::DRIVE);

if (isset($_SESSION['access_token'])) {

  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Drive($client);

  	if($archivo['error']==0){
  	$folderId="1AhtzIv1pWmP2y0BONmfxnPM1wa1dfW0T";
	//$nombre=$file['name'];
	move_uploaded_file($archivo['tmp_name'], '../files/'.$archivo['name']);

	 $fileMetadata = new Google_Service_Drive_DriveFile(array(
	    'name' => $archivo['name'],
		'parents' => array($folderId)));

	$content = file_get_contents('../files/'.$archivo['name']);

	$file = $service->files->create($fileMetadata, array(
	    'data' => $content,
	    'mimeType' => $archivo['type'],
	    'uploadType' => 'multipart',
	    'fields' => 'id'));

	//printf("File ID: %s\n", $file->id);
	chown('../files/'.$archivo['name'], 777);
	unlink('../files/'.$archivo['name']);
	header("location:../");
	}else{
		echo "hubo un error :(";
	}

} else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/drive/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}