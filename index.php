<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div style="display: flex;">
		<a href="formulario.php" style="margin: auto; width: 100%"><button type="button" class="btn btn-success" style="margin: auto; width: 100%"><b>Subir Archivo</b></button></a>
	</div>
<div style="display: flex;flex-direction: row;flex-wrap: wrap;justify-content: center;padding-top:40px;padding-bottom:40px;">

<?php
require_once __DIR__.'/vendor/autoload.php';

session_start();

$folderId="1AhtzIv1pWmP2y0BONmfxnPM1wa1dfW0T";
$client = new Google_Client();
$client->setAuthConfig('client_secret.json');
$client->addScope(Google_Service_Drive::DRIVE);

if (isset($_SESSION['access_token'])) {

try {
	$client->setAccessToken($_SESSION['access_token']);
	$service = new Google_Service_Drive($client);

  // Print the names and IDs for up to 10 files.
  $pageToken=null;
	$optParams = array(
		'q' => " '".$folderId."' in parents",
		//'q' => "not name contains 'jwlibrary'",
	  //'pageSize' => 40,
	  'spaces' => 'drive',
        'pageToken' => $pageToken,
        'fields' => 'nextPageToken, files(id, name,mimeType)',

	);
	$results = $service->files->listFiles($optParams);

	if (count($results->getFiles()) == 0) {
	    print "No files found.\n";
	} else {
	    foreach ($results->getFiles() as $file) {
	        ?>
	        		<div style="display: flex;flex-direction: column;margin: auto;width: 350px;height: 200px;box-shadow: 2px 2px 3px #999;border: 1px solid rgba(153, 153, 153, 0.3);justify-content: center;align-content: center;margin:10px;padding-bottom: 30px;">
	        			<div style="text-align: center;width: 95%;">
	        				<legend style="word-wrap: break-word;margin-top: 10px;"><?php echo $file->getName() ?></legend>
	        				<!-- h7><?php echo $file->getMimeType() ?></h7-->
	        			</div>
	        			<div style="margin: auto;"><a target="_blank" href="action/download.php?id=<?php echo $file->getId() ?>"><button type="button" class="btn btn-primary">Descargar</button></a></div>
	        		</div>
	        	
	        <?php
	    }
	}

} catch (Exception $e) {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/drive/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));	
}
} else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/drive/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}?>
</div>
</body>
</html>