<?php
require_once '../vendor/autoload.php';
session_start();

$client = new Google_Client();
$client->setAuthConfig('../client_secret.json');
$client->addScope(Google_Service_Drive::DRIVE_FILE);
$id=$_GET['id'];
if (isset($_SESSION['access_token'])) {

  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Drive($client);
  
  // Print the names and IDs for up to 10 files.
	$fileId = $id;
	$response = $service->files->get($fileId, array(
	    'alt' => 'media'));
	$type = $service->files->get($fileId);
	header("Content-Type: ".$type->getMimeType());
	header("Content-disposition: inline; filename=".$type->getName());
	$content = $response->getBody()->getContents();
	echo $content;
} else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/drive/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}